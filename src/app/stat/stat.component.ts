import { Component, OnInit, Input } from '@angular/core';
import { ApiServiceUser } from '../shared/api.service.user';

@Component({
  selector: 'app-stat',
  templateUrl: './stat.component.html',
  styleUrls: ['./stat.component.scss']
})
export class StatComponent implements OnInit {
  @Input() bgClass!: string;
  @Input() icon?: string;
  @Input() count?: string | number;
  // @Input() count?: number;
  @Input() label?: string;
  @Input() data?: number;

    // Comuni: Array<Comuni> = [];
    Comuni: any = [];
    
 comuniTotale?: string | number;

  constructor(private studentApi: ApiServiceUser) {
    this.studentApi.GetComuni().subscribe(data => {
      this.Comuni = data;
      this.comuniTotale= (Object.keys(this.Comuni).length).toString();
      console.log("COMUNI STAT:" , this.Comuni);
      console.log("comuniTotale:" , this.comuniTotale);
      
      
    })    
  }



  ngOnInit() {}
}
