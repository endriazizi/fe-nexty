import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { AppService } from '../app.service';
import { ApiServiceUser } from '../shared/api.service.user';
// import { NotifierService } from '../notifier.service';

import { UserService } from '../_services/user.service';

import { ApiServiceContatori } from '../shared/api.service.contatori';



interface Place {
  imgSrc: string;
  name: string;
  description: string;
  charge: string;
  location: string;
}

interface Comuni {
  id: string;
  nome: string;
  abitanti: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home-master.component.html',
  styleUrls: ['./home-master.component.scss']
})

export class HomeMasterComponent {
  /** Based on the screen size, switch from standard to one column per row */
  mio="CIAOOOO"
  cards = [];
  cardsForHandset = [];
  cardsForWeb = [];

 comuniTotale: number =0 ;


 
  isHandset: boolean = false;
  // isHandsetObserver: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(

  //   map(({ matches }) => {
  //     if (matches) {
  //       return true;
  //     }
  //     return false;
  //   })
  // );

  places: Array<Place> = [];

  
  // Comuni: Array<Comuni> = [];
  Comuni: any = [];
  

  countAllKits: any;
  countAllActiveKits: any;


  // constructor(private breakpointObserver: BreakpointObserver,
  //   public appService: AppService
  //   // private notifierService: NotifierService
  //   ) { }


  constructor(private contatoriApi: ApiServiceContatori) {
    // Count all kits
    this.contatoriApi.GetAllKits().subscribe(data => {
      this.countAllKits = data;

      console.log("Count all kits:" , this.countAllKits = data);
      
      
    }) 
    
    // Count active kits
    this.contatoriApi.GetActiveKits().subscribe(data => {
      this.countAllActiveKits = data;

      console.log("Count active kits:" , this.countAllActiveKits = data);
      
      
    })    
  }



  // constructor(private studentApi: ApiServiceUser) {
  //   this.studentApi.GetComuni().subscribe(data => {
  //     this.Comuni = data;
  //     this.comuniTotale= Object.keys(this.Comuni).length;
  //     console.log("COMUNI:" , this.Comuni);
  //     console.log("comuniTotale:" , this.comuniTotale);
      
      
  //   })    
  // }
  ngOnInit() {

    
    this.places = [
      {
        imgSrc: 'assets/images/ancona.jpg',
        name: 'Ancona',
        description: `The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the main night life in Barcelona.`,
        charge: '$899/night',
        location: 'Via Adige, 30'
      },
      {
        imgSrc: 'assets/images/jesi.jpg',
        name: 'Jesi',
        description: `The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the night life in London, UK.`,
        charge: 'Tributi riscossi $1,119',
        location: 'Via Carlo Magno, 12'
      },
      {
        imgSrc: 'assets/images/camerino.jpg',
        name: 'Camerino',
        description: `The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the main night life in Milan.`,
        charge: 'Tributi riscossi $459',
        location: 'Via Roma, 10'
      }
    ];


    // this.isHandsetObserver.subscribe(currentObserverValue => {
    //   this.isHandset = currentObserverValue;
    //   console.log("isHandset: ", this.isHandset);
    //   this.loadCards();
    //   console.log("cards: ", this.cards);
    // });

    // this.appService.getDeals().subscribe(
    //   response => {
    //     this.cardsForHandset = response.handsetCards;
    //     console.log("cardsForHandset: ", this.cardsForHandset);
    //     this.cardsForWeb = response.webCards;
    //     console.log("cardsForWeb: ", this.cardsForWeb);
    //     this.loadCards();
    //     // this.notifierService.showNotification('Todays deals loaded successfully. Click on any deal!', 'OK', 'success');

    //   },
    //   error => {
    //     alert('There was an error in receiving data from server. Please come again later!');
    //     // this.notifierService.showNotification('There was an error in receiving data from server!', 'OK', 'error');
    //   }
    // );

    
  }

  loadCards() {
    this.cards = this.isHandset ? this.cardsForHandset : this.cardsForWeb;
    console.log("cards loadCards(): ", this.cards);
  }

  getImage(imageName: string): string {
    return 'url(' + 'http://localhost:3000/images/' + imageName + '.jpg' + ')';
  }
}
